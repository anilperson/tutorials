# Notifications API
## Before we begin
WebKit is currently the only engine that supports the JavaScript Notifications API, so if you don't have a WebKit browser (i.e. [Google Chrome](http://google.com/chrome) or [Safari](http://apple.com/safari)) I suggest that you [download one](http://google.com/chrome), I myself prefer [Google Chrome](http://google.com/chrome) because it is updated very frequently.
## Uses
The Notifications API is meant for web apps like email services where you would want to notify a user if someone sent them an email, or social networks where if someone sends the user a direct message via the social network you would want to notify the user.
## Browser Support
Because the notifications API is currently only supported in WebKit browsers, we need to make sure that the user is using a compatible WebKit browser.


```
if (window.webkitNotifications) {
	alert('Your browser does support notifications!');
} else {
	alert('Your browser does not support notifications, :-( Use a WebKit browser (like Google Chrome) to see this in action.');
}
```
Our `if` statement checks to see if `window.webkitNotifications` returns true. If it does, then the user is using a WebKit browser where the notifications are supported. Then we tell the user that. If it returns false, then we give them a nice little message which tells them that notifications are not support in their browser and that they should use a WebKit browser.
## Permissions
Great! Now that we know that we will be able to create notifications, we need to check to see if we have permission from the user to create notifications. The permissions are important because you don't want any website to be able send you notifications. To check if we have permission we will use `window.webkitNotifications.checkPermission()`. `checkPermission()` will return one of three values:

* `0` - Allowed
* `1` - Undecided
* `2` - Denied

If `checkPermission()` returns 0 then we can create a notification. If it returns 1 then the user has not decided wether or not they will allow us to create notifications. So, if `checkPermission()` returns 2 then we can not create notifications. Inside of our first `if` loop that checks if the browser supports notifications lets add a second if loop. Our code should now look something like this:


```
if (window.webkitNotifications) {
	alert('Your browser does support notifications!');
	if (window.webkitNotifications.checkPermission() == 0) {
		alert('We can create a notification!');
	} else {
		window.webkitNotifications.requestPermission(function(){});
	}
} else {
	alert('Your browser does not support notifications, :-( Use a WebKit browser (like Google Chrome) to see this in action.');
}
```
Inside of the `if` statement we check if `checkPermission()` returns 0, and if it does then we tell the user. If it doesn't return 0 then we use `requestPermission()` to prompt the user do decide wether or not allow us to send them notifications or not.
**Note:** Safari needs callback function to be passed to `requestPermission()`, I have already done this here.
## Creating Notifications
Now we can actually create the notification. To do this we will use the function `createNotification()`. This function takes three parameters.

* `iconUrl` - The URL of the image to be used as the icon
* `title` - The title of the notification
* `body` - The text that will be displayed below the title

We will create our notification inside of a variable which will serve as the wrapper for our notification. This is what our notification will look like:

```
var notification = window.webkitNotifications.createNotification('my-icon.png', 'Something Happened', 'in our application');
notification.show();
```
`show()` will tell the browser to render our notification. We can also hide a notification use `cancel()`. There can be a queue of notifications because the browser can only show so many notifications at once on the screen. The `cancel()` function will also remove a notification from the queue. Finally lets add this to our current code inside of the if loop that checks wether or not we have permission.
## Notification Events
Notifications emit events (like onClick in normal JavaScript) during their lifecycle, these events are:

* `onclick` - When the user clicks on the notification. This is probably the most useful
* `onerror` - Will be triggered if there is an error in creating/showing the notification.
* `ondisplay` - Will be triggered when the notification becomes visible by the user.
* `onclose` - When the notification is closed by the user.

An example of how you would implement this would be:

```
notification.onClick(function() {
	alert('The notification was clicked on!');
});
```
## Finishing Up
Finally our code should look something like this:

```
if (window.webkitNotifications) {
	alert('Your browser does support notifications!');
	if (window.webkitNotifications.checkPermission() == 0) {
		alert('We can create a notification!');
		var notification = window.webkitNotifications.createNotification('my-icon.png', 'Something Happened', 'in our application');
		notification.show();
	} else {
		window.webkitNotifications.requestPermission(function(){});
	}
} else {
	alert('Your browser does not support notifications, :-( Use a WebKit browser (like Google Chrome) to see this in action.');
}
```

Check out my demo code on GitHub [here](http://github.com/anilperson/Tutorials). And watch [github.com/anilperson/Tutorials](http://github.com/anilperson/Tutorials) for more tutorials and demos.